package com.commercecore.scservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = "com.commercecore.scservice.api.clients")
@SpringBootApplication
public class ScserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScserviceApplication.class, args);
	}

}
