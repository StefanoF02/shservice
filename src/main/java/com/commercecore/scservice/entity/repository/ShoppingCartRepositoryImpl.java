package com.commercecore.scservice.entity.repository;

import com.commercecore.scservice.entity.ShoppingCart;
import com.commercecore.scservice.entity.ShoppingCartItem;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@AllArgsConstructor
@Repository
public class ShoppingCartRepositoryImpl implements ShoppingCartRepository{

    @Autowired
    private final EntityManager entityManager;


    @Override
    @Transactional
    public ShoppingCartItem createShoppingCartItem(ShoppingCartItem shoppingCartItem) {
        entityManager.persist(shoppingCartItem);
        return shoppingCartItem;
    }

}
