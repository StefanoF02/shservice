package com.commercecore.scservice.entity.repository;

import com.commercecore.scservice.entity.ShoppingCartItem;


public interface ShoppingCartRepository {

    ShoppingCartItem createShoppingCartItem(ShoppingCartItem shoppingCartItem);
}
