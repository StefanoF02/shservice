package com.commercecore.scservice.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Table(name = "shoppingcartitem")
@Entity
public class ShoppingCartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer itemId;

    private Long articleNumber;

    private String articleName;

    private Integer quantity;

    private Double price;

    @ManyToOne()
    @JoinColumn(name= "shoppingcart_id")
    private ShoppingCart shoppingCart;

}
