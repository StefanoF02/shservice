package com.commercecore.scservice.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
@Table(name = "shoppingcart")
public class ShoppingCart {

    @Id
    private Integer shoppingcartId;

    private String member;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "shoppingCart")
    @Builder.Default
    private Collection<ShoppingCartItem> shoppingCartItems = new HashSet<>();

}
