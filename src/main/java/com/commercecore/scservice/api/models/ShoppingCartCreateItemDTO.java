package com.commercecore.scservice.api.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ShoppingCartCreateItemDTO {
    private Long articleNumber;

    private Integer quantity;

}
