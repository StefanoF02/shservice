package com.commercecore.scservice.api.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ShoppingCartItemDTO {

    private Long articleNumber;

    private String articleName;

    private Integer quantity;

    private Double price;
}
