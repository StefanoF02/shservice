package com.commercecore.scservice.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDTO {

    private Long articleNumber;

    private String articleName;

    private Double price;

    private String producer;

    private Integer stock;

    private String categories;

    private String currency;

    private Collection<String> reviews = new ArrayList<String>();

}
