package com.commercecore.scservice.api.clients;

import com.commercecore.scservice.api.models.ProductDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("PRODUCTSERVICE")
public interface ProductServiceClient {

    @GetMapping("/products/{articleNumber}")
    public ResponseEntity<ProductDTO> getProduct(@PathVariable Long articleNumber);
}
