package com.commercecore.scservice.api.service;

import com.commercecore.scservice.api.clients.ProductServiceClient;
import com.commercecore.scservice.api.models.ProductDTO;
import com.commercecore.scservice.api.models.ShoppingCartCreateItemDTO;
import com.commercecore.scservice.entity.ShoppingCartItem;
import com.commercecore.scservice.entity.repository.ShoppingCartRepository;
import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ShoppingCartService {

    @Autowired
    private final ShoppingCartRepository shoppingCartRepository;

    @Autowired
    ProductServiceClient productServiceClient;


    public ShoppingCartItem createShoppingCartItem(ShoppingCartCreateItemDTO shoppingCartCreateItemDTO){

        val shoppingCartItem = buildItem(shoppingCartCreateItemDTO);

        shoppingCartRepository.createShoppingCartItem(shoppingCartItem);

        return shoppingCartItem;
    }


    private ShoppingCartItem buildItem(ShoppingCartCreateItemDTO shoppingCartCreateItemDTO){
        ProductDTO product = productServiceClient.getProduct(shoppingCartCreateItemDTO.getArticleNumber()).getBody();

        if(product == null){
            return null;
        }

        return ShoppingCartItem.builder()
                .articleNumber(shoppingCartCreateItemDTO.getArticleNumber())
                .articleName(product.getArticleName())
                .price(product.getPrice())
                .quantity(shoppingCartCreateItemDTO.getQuantity())
                .build();


    }

}
