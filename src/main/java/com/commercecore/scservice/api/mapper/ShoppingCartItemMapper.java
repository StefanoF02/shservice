package com.commercecore.scservice.api.mapper;

import com.commercecore.scservice.api.models.ShoppingCartItemDTO;
import com.commercecore.scservice.entity.ShoppingCartItem;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ShoppingCartItemMapper {
    public ShoppingCartItem toDomain(ShoppingCartItemDTO shoppingCartItemDTO);

    public ShoppingCartItemDTO toApi(ShoppingCartItem shoppingCartItem);
}
