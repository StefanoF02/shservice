package com.commercecore.scservice.api.controller;

import com.commercecore.scservice.api.mapper.ShoppingCartItemMapper;
import com.commercecore.scservice.api.models.ShoppingCartCreateItemDTO;
import com.commercecore.scservice.api.models.ShoppingCartItemDTO;
import com.commercecore.scservice.api.service.ShoppingCartService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/shoppingcart")
public class ShoppingCartController {

    @Autowired
    private final ShoppingCartService shoppingCartService;

    @Autowired
    private final ShoppingCartItemMapper shoppingCartItemMapper;


    @PostMapping()
    public ResponseEntity<ShoppingCartItemDTO> saveProduct(
            @RequestBody ShoppingCartCreateItemDTO shoppingCartCreateItemDTO){
        var item = shoppingCartService.createShoppingCartItem(shoppingCartCreateItemDTO);

        return new ResponseEntity<>(shoppingCartItemMapper.toApi(item), HttpStatus.OK);
    }
}
